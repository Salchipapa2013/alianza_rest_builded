/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/things              ->  index
 * POST    /api/things              ->  create
 * GET     /api/things/:id          ->  show
 * PUT     /api/things/:id          ->  update
 * DELETE  /api/things/:id          ->  destroy
 */

'use strict';

var _ = require('lodash');
var Thing = require('./thing.model');

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

function responseWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function saveUpdates(updates) {
  return function(entity) {
    var updated = _.merge(entity, updates);
    return updated.saveAsync()
      .spread(function(updated) {
        return updated;
      });
  };
}




function removeEntity(res) {
  return function(entity) {
    if (entity) {
      return entity.removeAsync()
        .then(function() {
          res.status(204).end();
        });
    }
  };
}

// Gets a list of Things
exports.index = function(req, res) {  
  var nodemailer = require('nodemailer');
  var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'admin.informativo@laalianzacristiana.co',
        pass: 'XLPD$%&124887'
    }
  });
  var message = '<h1>Nuevo Postulado:</h1>'+
                '<strong>Nombre: </strong> '+req.body.nombre+
                '<br><br><strong>Apellidos: </strong> '+req.body.apellidos+
                '<br><br><strong>Telefono: </strong> '+req.body.telefono+
                '<br><br><strong>Celular: </strong> '+req.body.celular+
                '<br><br><strong>Email: </strong> '+req.body.email+
                '<br><br><strong>Sede: </strong> '+req.body.sede+
                '<br><br><strong>Región: </strong> '+req.body.region;
  transporter.sendMail({
      from: 'admin.informativo@laalianzacristiana.co',
      to: 'felipe.valencia@laalianzacristiana.co,absimpson@laalianzacristiana.co',
      subject: '(Nueva) A.B Simpson Postulación',
      html:message 
  });
  /*res.status(200).send({
    'nombre':req.body.nombre,
    'apellidos':req.body.apellidos,
    'telefono':req.body.telefono,
    'celular':req.body.celular,
    'email':req.body.email,
    'sede':req.body.sede,
    'region':req.body.region
  });
  /*console.log({
    'nombre':req.body.nombre,
    'apellidos':req.body.apellidos,
    'telefono':req.body.telefono,
    'celular':req.body.celular,
    'email':req.body.email,
    'sede':req.body.sede,
    'region':req.body.region
  });*/
  res.redirect('https://www.google.com.co/');
};

// Gets a single Thing from the DB
exports.show = function(req, res) {
  Thing.findByIdAsync(req.params.id)
    .then(handleEntityNotFound(res))
    .then(responseWithResult(res))
    .catch(handleError(res));
};

// Creates a new Thing in the DB
exports.create = function(req, res) {
  Thing.createAsync(req.body)
    .then(responseWithResult(res, 201))
    .catch(handleError(res));
};

// Updates an existing Thing in the DB
exports.update = function(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  Thing.findByIdAsync(req.params.id)
    .then(handleEntityNotFound(res))
    .then(saveUpdates(req.body))
    .then(responseWithResult(res))
    .catch(handleError(res));
};

// Deletes a Thing from the DB
exports.destroy = function(req, res) {
  Thing.findByIdAsync(req.params.id)
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
};
