'use strict';

var express = require('express');
var controller = require('./thing.controller');

var router = express.Router();

//Api para absipmson, envio de formularios.
router.post('/absipmson/send', controller.index);
/*
router.get('/:id', controller.show);
router.post('/', controller.create);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);*/

module.exports = router;
